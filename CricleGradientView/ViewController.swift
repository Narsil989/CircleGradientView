//
//  ViewController.swift
//  CricleGradientView
//
//  Created by Dejan Kraguljac on 16/01/2018.
//  Copyright © 2018 Dejan Kraguljac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let spinner: SpinnerView = {
        
        let view = SpinnerView(circleRadious: 50, startAngle: -CGFloat(Double.pi/2), endAngle: (CGFloat(Double.pi*3/2)), clockwise: true)
        view.drawLineWidth = 10
        view.emptyLineWidth = 10
        view.animationOption = .drawingAndRotation
        view.rotationDuration = 2
        view.emptyLineColor = .clear//UIColor.init(red: 16/255, green: 29/255, blue: 64/255, alpha: 1)
        
        view.image = nil
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.spinner)
        self.spinner.translatesAutoresizingMaskIntoConstraints = false
        self.spinner.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.spinner.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.spinner.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.spinner.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        self.spinner.spinnerCenterPoint = CGPoint(x: UIScreen.main.bounds.size.width/2, y: UIScreen.main.bounds.size.height/2)
        self.spinner.animateSpinnerView()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

