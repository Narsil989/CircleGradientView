
import UIKit

enum ColorBrightness {
    case light
    case dark
}

extension UIColor {
    
    static let lightBackgroundColor = UIColor.red
    static let darkBackgroudColor = UIColor.blue
    
    // based on https://www.w3.org/WAI/ER/WD-AERT/#color-contrast
    func isLight() -> Bool {
        
        guard let components = cgColor.components,
            let redComponent = components[safe: 0],
            let greenComponent = components[safe: 1],
            let blueComponent = components[safe: 2] else {
                return false
        }
        
        let redBrightness = redComponent * 299
        let greenBrightness = greenComponent * 587
        let blueBrightness = blueComponent * 114
        
        let brightness = (redBrightness + greenBrightness + blueBrightness) / 1000
        return brightness > 0.5
    }
    
    static func contrastingColor(basedOff color: UIColor) -> UIColor {
        return color.isLight() ? UIColor.darkBackgroudColor : UIColor.lightBackgroundColor
    }
    
    /// Used for combinig 2 colors based on percentage
    static func mixColors(previousColor: UIColor, nextColor: UIColor, percentage: CGFloat, alpha: CGFloat = 1) -> UIColor {
        
        let previousColorComponents = previousColor.cgColor.components!
        let nextColorComponents = nextColor.cgColor.components!
        
        var redComponent: CGFloat = 0
        var greenComponent: CGFloat = 0
        var blueComponent: CGFloat = 0
        
        // calculate red color
        if (previousColorComponents[0] > nextColorComponents[0]) {
            redComponent = previousColorComponents[0] - (previousColorComponents[0] - nextColorComponents[0]) * percentage
            
        } else if (previousColorComponents[0] < nextColorComponents[0]){
            redComponent = previousColorComponents[0] + (nextColorComponents[0] - previousColorComponents[0]) * percentage
            
        } else {
            redComponent = previousColorComponents[0]
        }
        
        // calculate green color
        if (previousColorComponents[1] > nextColorComponents[1]) {
            greenComponent = previousColorComponents[1] - (previousColorComponents[1] - nextColorComponents[1]) * percentage
            
        } else if(previousColorComponents[1] < nextColorComponents[1]){
            greenComponent = previousColorComponents[1] + (nextColorComponents[1] - previousColorComponents[1]) * percentage
            
        } else {
            greenComponent = previousColorComponents[1]
        }
        
        // calculate blue color
        if (previousColorComponents[2] > nextColorComponents[2]) {
            blueComponent = previousColorComponents[2] - (previousColorComponents[2] - nextColorComponents[2]) * percentage
            
        } else if (previousColorComponents[2] < nextColorComponents[2]) {
            blueComponent = previousColorComponents[2] + (nextColorComponents[2] - previousColorComponents[2]) * percentage
            
        } else {
            blueComponent = previousColorComponents[2]
        }
        
        // combine calculated color components
        return UIColor(red: redComponent, green: greenComponent, blue: blueComponent, alpha: alpha)
    }
    
    
}

extension Array {
    subscript(safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
}
