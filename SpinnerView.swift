//
//  SpinnerView.swift
//  SpinnerView
//
//  Copyright © 2017 Dejan Kraguljac. All rights reserved.
//

import UIKit

enum SpinnerState {
    case finish
    case start
}

extension CGLineCap {
    var stringLineCapStyle: String {
        
        switch self {
        case .round:
            return "round"
        case .butt:
            return "butt"
        case .square:
            return "square"
        }
    }
}

enum AnimationOption {
    case rotation
    case drawing
    case drawingAndRotation
}

open class SpinnerView: UIView {

    var circleRadious: CGFloat = 20
    var spinnerCenterPoint = CGPoint(x: 10, y: 10)
    var startAngle: CGFloat = 0
    var endAngle: CGFloat = (CGFloat(CGFloat.pi * 2))
    var clockwise = true
    var lineCapStyle: CGLineCap = .round
    var animationDuration = 1.0
    var emptyLineWidth = 16.0 //usually should be the same as draw line width... but you never know....designers...
    var drawLineWidth = 16.0
    var emptyLineColor = UIColor.gray
    var drawLineColor = UIColor.red
    var animationOption: AnimationOption = .rotation
    
    var startGradientColor: UIColor = .blue
    var endGradientColor: UIColor = .red
    var spinnerRect = CGRect.zero
    
    //roration parameters
    var rotationDuration = 2.0
    var rotationRepeatDuration = CGFloat.greatestFiniteMagnitude
    
    var state:SpinnerState = .start
    var image: UIImage? = UIImage(named: "logInSignUpLogo")
    
    var animationDidComplete:(()->Void)?
    
    //MARK: - Init setup
    init(circleRadious: CGFloat, startAngle: CGFloat, endAngle: CGFloat, clockwise: Bool)
    {
        super.init(frame: .zero)
        self.circleRadious = circleRadious
        self.startAngle = startAngle
        self.endAngle = endAngle
        self.clockwise = clockwise
    }
    
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Start animation
    
    func animateSpinnerView()
    {
        self.removePreviousSublayers()
        
        let mixColor = UIColor.mixColors(previousColor: self.startGradientColor, nextColor: self.endGradientColor, percentage: 0.55)
        
        self.addEmptyLineLayer()
        
        if self.image != nil {
            self.addLogoImageView()
        }
        
        self.calculateSpinnerRect()
        
        self.animateSpinnerViewWith(startColor: startGradientColor, endColor: mixColor, startAngle: self.startAngle, endAngle: CGFloat(CGFloat.pi/2), startPoint: CGPoint(x: 0.0, y: 0.0), endPoint: CGPoint(x: 0.0, y: 1.0), tag:0, gradientBounds: spinnerRect)
    }
    
    
    //this will start animation according to animation option enum
    func animateSpinnerViewWith(startColor: UIColor, endColor: UIColor, startAngle: CGFloat, endAngle: CGFloat, startPoint: CGPoint, endPoint: CGPoint, tag: Int, gradientBounds: CGRect)
    {
        if (self.animationOption == .drawing || self.animationOption == .drawingAndRotation)
        {
            //first draw path
            let circlePath = UIBezierPath(arcCenter: CGPoint(x: self.spinnerCenterPoint.x, y: self.spinnerCenterPoint.y), radius: self.circleRadious, startAngle: startAngle, endAngle:endAngle, clockwise: self.clockwise)
            //get shape layer according to drawn path
            let shapeLayer = self.getShapeLayer(layerPath: circlePath)
            //get animation for drawing
            let animation = self.drawingAnimation(duration: 1.0, timingFunction: CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear))//tag == 0 ? CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn) : CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut))
            //only add first animation as delegate since we want to continue animating second part od circle after first is finished
//            if tag == 0 {
                animation.delegate = self
//            }
            
            if tag == 1 {
                self.state = .finish
            }
            
            //load gradient layer and set shape layer as mask
            self.layer.addSublayer(self.getGradientLayer(gradientPosition: CGPoint(x: self.spinnerCenterPoint.x, y: self.spinnerCenterPoint.y),
                                                         layerStartPoint: startPoint,
                                                         layerEndPoint: endPoint,
                                                         startColor: startColor,
                                                         endColor: endColor,
                                                         maskLayer: shapeLayer))
            shapeLayer.strokeEnd = 1.0
            shapeLayer.add(animation, forKey: "animateCircle")
            
            
        }
        else
        {
            //rotation animation is excluded from here because of drawing angles
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.loadRotatingAnimation()
            }
        }
        
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    //MARK: Drawing animation
    
    fileprivate func drawingAnimation(duration: TimeInterval, timingFunction: CAMediaTimingFunction) -> CABasicAnimation {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = duration
        animation.fromValue = 0
        animation.toValue = 1
        animation.timingFunction = timingFunction
        
        return animation
    }
    
    //MARK: - Load layers needed for raotation animation, animation start
    
    fileprivate func loadRotatingAnimation()
    {
        self.removePreviousSublayers()
        
        //get all the colors
        let mixColorRight = UIColor.mixColors(previousColor: self.startGradientColor, nextColor: self.endGradientColor, percentage: 0.55)
        let mixColorLeft = UIColor.mixColors(previousColor: self.startGradientColor, nextColor: self.endGradientColor, percentage: 0.45)
        
        //draw right path first
        let circlePathRight = UIBezierPath(arcCenter: CGPoint(x: self.spinnerCenterPoint.x, y: self.spinnerCenterPoint.y), radius: self.circleRadious, startAngle: self.startAngle, endAngle:CGFloat(CGFloat.pi/2), clockwise: self.clockwise)
        let shapeLayerRight = self.getShapeLayer(layerPath: circlePathRight)
        
        self.layer.addSublayer(self.getGradientLayer(gradientPosition: CGPoint(x: self.spinnerCenterPoint.x, y: self.spinnerCenterPoint.y),
                                                     layerStartPoint: CGPoint(x: 0.0, y: 0.0),
                                                     layerEndPoint: CGPoint(x: 0.0, y: 1.0),
                                                     startColor: self.startGradientColor,
                                                     endColor: mixColorRight,
                                                     maskLayer: shapeLayerRight))
        //draw the left path
        let circlePathLeft = UIBezierPath(arcCenter: CGPoint(x: self.spinnerCenterPoint.x, y: self.spinnerCenterPoint.y), radius: self.circleRadious, startAngle: CGFloat(CGFloat.pi/2), endAngle:CGFloat(CGFloat.pi*3/2), clockwise: self.clockwise)
        let shapeLayerLeft = self.getShapeLayer(layerPath: circlePathLeft)
        
        self.layer.addSublayer(self.getGradientLayer(gradientPosition: CGPoint(x: self.spinnerCenterPoint.x, y: self.spinnerCenterPoint.y),
                                                     layerStartPoint: CGPoint(x: 0.0, y: 1.0),
                                                     layerEndPoint: CGPoint(x: 0.0, y: 0.0),
                                                     startColor: mixColorLeft,
                                                     endColor: self.endGradientColor,
                                                     maskLayer: shapeLayerLeft))
        //add rotating animation

        for layer in self.layer.sublayers!
        {
            if (layer is CAGradientLayer)
            {
                layer.add(self.rotatingAnimation(), forKey: "rotation")
            }
        }
    }
    
    fileprivate func rotatingAnimation() -> CABasicAnimation
    {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.duration = self.rotationDuration
        animation.fromValue = 0
        animation.repeatDuration = CFTimeInterval(self.rotationRepeatDuration)
        animation.toValue = CGFloat(CGFloat.pi*2)
        animation.isRemovedOnCompletion = false
        return animation
    }
    
    //MARK: - Helpers
    
    
    //MARK: Add image logo
    func addLogoImageView()
    {
        let imageView = UIImageView(image: self.image)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imageView)
        
        let horizontalConstraint = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 90)
        let heightConstraint = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 128)
        
        self.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
    }
    
    //MARK: Get gradient layer
    
    fileprivate func getGradientLayer(gradientPosition: CGPoint, layerStartPoint: CGPoint, layerEndPoint: CGPoint, startColor: UIColor, endColor: UIColor, maskLayer: CAShapeLayer) -> CAGradientLayer
    {
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.contentsScale = UIScreen.main.scale
        gradientLayer.position = gradientPosition
        gradientLayer.bounds = spinnerRect
        gradientLayer.startPoint = layerStartPoint
        gradientLayer.endPoint = layerEndPoint
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        gradientLayer.mask = maskLayer
        
        return gradientLayer
    }
    
    //MARK: Get shape layer
    
    fileprivate func getShapeLayer(layerPath: UIBezierPath) -> CAShapeLayer
    {
        let shapeLayer = CAShapeLayer()
        shapeLayer.contentsScale = UIScreen.main.scale
        shapeLayer.path = layerPath.cgPath
        shapeLayer.lineCap = self.lineCapStyle.stringLineCapStyle
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = self.drawLineColor.cgColor
        shapeLayer.lineWidth = CGFloat(self.drawLineWidth)
        
        return shapeLayer
    }
    
    //MARK: Other
    
    fileprivate func removePreviousSublayers()
    {
        guard ((self.layer.sublayers?.count) != nil) else {
            
            return
        }
        
        self.layer.removeAllAnimations()
        
        for layer in self.layer.sublayers!
        {
            if (layer is CAShapeLayer || layer is CAGradientLayer)
            {
                layer.removeFromSuperlayer()
            }
        }
    }
    
    func stopAnimations() {
        
        self.layer.removeAllAnimations()
    }
    
    fileprivate func calculateSpinnerRect()
    {
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: self.spinnerCenterPoint.x, y: self.spinnerCenterPoint.y), radius: self.circleRadious, startAngle: self.startAngle, endAngle:self.endAngle, clockwise: self.clockwise)
        spinnerRect = CGRect(x: circlePath.bounds.origin.x - CGFloat(self.drawLineWidth*3/4), y: circlePath.bounds.origin.y - CGFloat(self.drawLineWidth*3/4), width: circlePath.bounds.size.width+CGFloat(self.drawLineWidth*3/2), height: circlePath.bounds.size.height+CGFloat(self.drawLineWidth*3/2))
    }
    
    //MARK: Add empty line
    
    fileprivate func addEmptyLineLayer()
    {
        let emptyLineShapeLayer = CAShapeLayer()
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: self.spinnerCenterPoint.x, y: self.spinnerCenterPoint.y), radius: self.circleRadious, startAngle: self.startAngle, endAngle:self.endAngle, clockwise: self.clockwise)
        emptyLineShapeLayer.path = circlePath.cgPath
        emptyLineShapeLayer.fillColor = UIColor.clear.cgColor
        emptyLineShapeLayer.strokeColor = self.emptyLineColor.cgColor
        emptyLineShapeLayer.lineWidth = CGFloat(self.emptyLineWidth)
        self.layer.addSublayer(emptyLineShapeLayer)
    }
    
    deinit {
        self.removePreviousSublayers()
        self.layer.removeAllAnimations()
        print("deinit: \(String(describing: self))")
    }
}

//MARK: - Animation delegate

extension SpinnerView: CAAnimationDelegate {
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag == true, self.animationOption == .drawing  || self.animationOption == .drawingAndRotation{
            
            if self.state == .finish {
                // scaleUp animation has completed
                print("Finish!!!!!")
                self.state = .start
                if let action = self.animationDidComplete {
                    action()
                }
                
                if self.animationOption == .drawingAndRotation {
                    
                    for layer in self.layer.sublayers!
                    {
                        if (layer is CAGradientLayer)
                        {
                            layer.add(self.rotatingAnimation(), forKey: "rotation")
                        }
                    }
                }
            } else {
                print("Nothing!!")
                let mixColor = UIColor.mixColors(previousColor: self.startGradientColor, nextColor: self.endGradientColor, percentage: 0.45)
                
                self.animateSpinnerViewWith(startColor: mixColor, endColor: self.endGradientColor, startAngle: CGFloat(CGFloat.pi/2), endAngle: CGFloat(CGFloat.pi*3/2), startPoint: CGPoint(x: 0.0, y: 1.0), endPoint: CGPoint(x: 0.0, y: 0.0), tag:1, gradientBounds: spinnerRect)
            }
        }
        else
        {
            
        }
    }
    
}
